$(document).ready(function () {
    var LEGO = (function () {
        var COMPONENTS_PATH = './../../components/';
        return {
            init: function () {
                var componentsList = $('body').find("[data-component]");
                console.log(componentsList);
                for (var i = 0; i < componentsList.length; i++) {
                    this.loadComponentHTML(componentsList[i]);
                }
            },
            loadComponentHTML: function (component) {
                var componentKey = component.getAttribute("data-component");
                var component = $("[data-component='" + componentKey + "']");
                component.setAttribute("data-resolved", true);
                component.load(COMPONENTS_PATH + componentKey + '/' + componentKey + '.html');
            }
        };
    })();

    LEGO.init();
});